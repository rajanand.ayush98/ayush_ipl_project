// import required modules
const csv = require('csvtojson')
const fs = require('fs')

// path to csv files
const matchFilePath = './data/matches.csv'
const deliveriesFilePath = './data/deliveries.csv'

// read csv files
async function convertCsvtoJSON(csvFilePath){
    const csvData = await csv().fromFile(csvFilePath)
    const outFilePath = modifyFilePath(csvFilePath)
    fs.writeFileSync(outFilePath, JSON.stringify(csvData))
}

// set the path for json files
const modifyFilePath = filename => filename.replace('.csv', '.json')


convertCsvtoJSON(matchFilePath)
convertCsvtoJSON(deliveriesFilePath)