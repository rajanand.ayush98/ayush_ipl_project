// importing required modules
const fs = require('fs')

// Number of matches played per year for all the years in IPL.
exports.matchesPlayed = (jsonObjMatches)=>{
    let matchSeason = {}
    jsonObjMatches.forEach(element => {
        let season = element.season
        if(season in matchSeason){
            // increase the value of the key
            matchSeason[season] += 1
        }
        else{
            // add element to the Object   
            matchSeason[season] = 1
        }
      });

    // console.log(seasonArray)
    writeFile(matchSeason, 'matchSeason.json')
}


// Number of matches won per team per year in IPL.
exports.matchesWon = (jsonObjMatches)=>{
    let matchesWonObj = {}
    // console.log(jsonObj.slice(0,1))
    jsonObjMatches.forEach(element => {
        let season = element.season
        let winner = element.winner
        if(season in matchesWonObj){
            // increase the value of the key
            if(winner in matchesWonObj[season]){
                matchesWonObj[season][winner] += 1
            }
            else{
                matchesWonObj[season][winner] = 1
            }   
        }
        else{
            // initialise the inner object   
            matchesWonObj[season] = {}
        }
    });

    // console.log(matchesWonObj)
    writeFile(matchesWonObj, 'matchesWon.json')
}

function getMatchId(obj, year){
    let yearArray = []
    obj.forEach(element => {
        if(element.season == year){
            yearArray.push(element.id)
        }
    });
    return yearArray
}

// Extra runs conceded per team in the year 2016
exports.extraRuns = (jsonObjMatches, jsonObjDeliveries)=>{
    let extraRunsObj = {}
    
    let yearArray = getMatchId(jsonObjMatches, 2016)
    jsonObjDeliveries.forEach(element => {
            if (element['match_id'] in yearArray ) {
                let team = element['bowling_team']
                let extraRuns = element['extra_runs']
                if(team in extraRunsObj){
                    extraRunsObj[team] += parseInt(extraRuns)
                }
                else{
                    extraRunsObj[team] = parseInt(extraRuns)
                }  
            }
    });
    // console.log(extraRunsObj)   
    writeFile(extraRunsObj, 'extraRuns.json')
}


// Top 10 economical bowlers in the year 2015
exports.topBowlers = (jsonObjMatches, jsonObjDeliveries)=>{
    let yearArray = getMatchId(jsonObjMatches, 2015)
    let bowlers = {}
    jsonObjDeliveries.forEach(element => {
        if(element['match_id'] in yearArray){
            let bowlerName = element['bowler']
            if(bowlerName in bowlers){
                bowlers[bowlerName] += element.total_runs/element.ball;
            }
            else{
                bowlers[bowlerName] = element.total_runs/element.ball;
            }
        }
    });

    let bowlerSorted = Object.keys(bowlers).sort((a, b) => {
        return bowlers[a] - bowlers[b];
      })
    //   console.log(bowlerSorted.slice(0,10));
      writeFile(bowlerSorted.slice(0,10), 'bowlers.json')
    // console.log(bowlers)

}


// write to a json file
function writeFile(data, fileName){
    let path = './public/output/'+fileName
    const jsonString = JSON.stringify(data)
    fs.writeFileSync(path, jsonString)
}