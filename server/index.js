// importing required modules
const ipl = require('./ipl.js')
const fs = require('fs')

// fectching data from json file
const jsonFileMatches = JSON.parse(fs.readFileSync('./data/matches.json'))
const jsonFileDeliveries = JSON.parse(fs.readFileSync('./data/deliveries.json'))

// main function to do the stuffs
function main(){
  ipl.matchesPlayed(jsonFileMatches)
  ipl.matchesWon(jsonFileMatches)
  ipl.extraRuns(jsonFileMatches, jsonFileDeliveries)
  ipl.topBowlers(jsonFileMatches, jsonFileDeliveries)
  ipl.wonMatchToss(jsonFileMatches)
  ipl.playerOfMatch(jsonFileMatches)
}

main()