// importing required modules
const fs = require('fs')

// Number of matches played per year for all the years in IPL.
exports.matchesPlayed = (jsonObjMatches)=>{
    var seasonArray = jsonObjMatches.reduce((ac, element)=>{
        ac[element.season] ? ac[element.season] +=1 : ac[element.season] = 1
        return ac
        
    },{})
    // approach 2
    // let seasonArray = jsonObjMatches.reduce((ac, element)=>{
    //     ac[element.season] =++ac[element.season] || 1;
    //     return ac;
    // },{})
    // console.log(seasonArray)
    writeFile(seasonArray, 'matchSeason.json')
}


// Number of matches won per team per year in IPL.
exports.matchesWon = (jsonObjMatches)=>{
    let matchesWonObj = jsonObjMatches.map(element => {
        return {season: element.season, winner: element.winner}
    }).reduce((matchesWon, element)=>{
        let season = element.season
        let winner = element.winner
        if(element.season in matchesWon){
            if(winner in matchesWon[season]){
                matchesWon[season][winner] += 1
            }
            else{
                matchesWon[season][winner] = 1
            }
        }
        else{
            matchesWon[season] = {}
        }
        return matchesWon
    },{})
    // console.log(matchesWonObj)
    writeFile(matchesWonObj, 'matchesWon.json')
}


// Extra runs conceded per team in the year 2016
exports.extraRuns = (jsonObjMatches, jsonObjDeliveries)=>{    
    let yearArray = getMatchId(jsonObjMatches, 2016)
    const extraRunsObj = jsonObjDeliveries.filter(element=>element['match_id'] in yearArray)
    .reduce((match, element)=>{
        let team = element['bowling_team']
        let extraRuns = element['extra_runs']
        if(match[team]){
            match[team] += parseInt(extraRuns)
        }
        else{
            match[team] = parseInt(extraRuns)
        }
        return match  
    },{})
    // console.log(extraRunsObj)   
    writeFile(extraRunsObj, 'extraRuns.json')
}


// Top 10 economical bowlers in the year 2015
exports.topBowlers = (jsonObjMatches, jsonObjDeliveries)=>{
    let yearArray = getMatchId(jsonObjMatches, 2015)
    let bowlers = jsonObjDeliveries.filter(element => element['match_id'] in yearArray)
    .reduce((bowlerObj, element)=>{
        let bowlerName = element['bowler']
        if(bowlerName in bowlerObj){
            bowlerObj[bowlerName] += element.total_runs/element.ball;
        }
        else{
            bowlerObj[bowlerName] = element.total_runs/element.ball;
        }

        return bowlerObj
    },{})
    var bowlerArray = sortObject(bowlers)
    writeFile(bowlerArray.slice(0,10), 'bowlers.json')
    // console.log(bowlerArray)
}


// Find the number of times each team won the toss and also won the match
exports.wonMatchToss = (jsonObjMatches)=>{
    var timesWon = jsonObjMatches.filter((element)=>{
        return element.toss_winner == element.winner
    }).reduce((match, element)=>{
        var team =  element.winner
        if(match[team]){
            match[team] +=1
        }
        else{
            match[team] = 1
        }
        return match
    },{})

    // console.log(timesWon)
    writeFile(timesWon, 'timesWonTossMatch.json')

}


// Find a player who has won the highest number of Player of the Match awards for each season
exports.playerOfMatch = (jsonObjMatches)=>{
    var playerOfMatch = jsonObjMatches.reduce((player, element)=>{
        var name = element.player_of_match
        player[name] ? player[name] += 1 : player[name] =1
        return player
    },{})
    var playerArray =  sortObject(playerOfMatch)
    writeFile(playerArray[0], 'playerOfMatch.json')
    // console.log(playerArray[0])
}


// to get matches id of particular year
function getMatchId(obj, year){
    const  yearArray = obj.filter(matches=> matches.season == year).map(element => element.id)
    return yearArray
}


// write to a json file
function writeFile(data, fileName){
    let path = './public/output/'+fileName
    const jsonString = JSON.stringify(data)
    fs.writeFileSync(path, jsonString)
}


// convert into array and sort the object
function sortObject(obj){
    var objKeys = Object.keys(obj)
    var dataArray = []
    objKeys.forEach(element => {
        dataArray.push([element, obj[element]])
    });

    dataArray.sort(function([,a], [,b]){return b - a});
    return dataArray
}