# Ayush_IPL_Project

In this data assignment you will transform raw data of IPL to calculate the following stats:

1. Number of matches played per year for all the years in IPL.
2. Number of matches won per team per year in IPL.
3. Extra runs conceded per team in the year 2016
4. Top 10 economical bowlers in the year 2015
5. Find the number of times each team won the toss and also won the match
6. Find a player who has won the highest number of Player of the Match awards for each season
Implement the 4 functions, one for each task. Use the results of the functions to dump JSON files in the output folder